require 'test_helper'

class EegFilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @eeg_file = eeg_files(:one)
  end

  test "should get index" do
    get eeg_files_url
    assert_response :success
  end

  test "should get new" do
    get new_eeg_file_url
    assert_response :success
  end

  test "should create eeg_file" do
    assert_difference('EegFile.count') do
      post eeg_files_url, params: { eeg_file: { date: @eeg_file.date, de_identified: @eeg_file.de_identified, first_name: @eeg_file.first_name, host: @eeg_file.host, last_name: @eeg_file.last_name, middle_name: @eeg_file.middle_name, mrn: @eeg_file.mrn, path: @eeg_file.path, type: @eeg_file.type } }
    end

    assert_redirected_to eeg_file_url(EegFile.last)
  end

  test "should show eeg_file" do
    get eeg_file_url(@eeg_file)
    assert_response :success
  end

  test "should get edit" do
    get edit_eeg_file_url(@eeg_file)
    assert_response :success
  end

  test "should update eeg_file" do
    patch eeg_file_url(@eeg_file), params: { eeg_file: { date: @eeg_file.date, de_identified: @eeg_file.de_identified, first_name: @eeg_file.first_name, host: @eeg_file.host, last_name: @eeg_file.last_name, middle_name: @eeg_file.middle_name, mrn: @eeg_file.mrn, path: @eeg_file.path, type: @eeg_file.type } }
    assert_redirected_to eeg_file_url(@eeg_file)
  end

  test "should destroy eeg_file" do
    assert_difference('EegFile.count', -1) do
      delete eeg_file_url(@eeg_file)
    end

    assert_redirected_to eeg_files_url
  end
end
