require "application_system_test_case"

class EegFilesTest < ApplicationSystemTestCase
  setup do
    @eeg_file = eeg_files(:one)
  end

  test "visiting the index" do
    visit eeg_files_url
    assert_selector "h1", text: "Eeg Files"
  end

  test "creating a Eeg file" do
    visit eeg_files_url
    click_on "New Eeg File"

    fill_in "Date", with: @eeg_file.date
    fill_in "De identified", with: @eeg_file.de_identified
    fill_in "First name", with: @eeg_file.first_name
    fill_in "Host", with: @eeg_file.host
    fill_in "Last name", with: @eeg_file.last_name
    fill_in "Middle name", with: @eeg_file.middle_name
    fill_in "Mrn", with: @eeg_file.mrn
    fill_in "Path", with: @eeg_file.path
    fill_in "Type", with: @eeg_file.type
    click_on "Create Eeg file"

    assert_text "Eeg file was successfully created"
    click_on "Back"
  end

  test "updating a Eeg file" do
    visit eeg_files_url
    click_on "Edit", match: :first

    fill_in "Date", with: @eeg_file.date
    fill_in "De identified", with: @eeg_file.de_identified
    fill_in "First name", with: @eeg_file.first_name
    fill_in "Host", with: @eeg_file.host
    fill_in "Last name", with: @eeg_file.last_name
    fill_in "Middle name", with: @eeg_file.middle_name
    fill_in "Mrn", with: @eeg_file.mrn
    fill_in "Path", with: @eeg_file.path
    fill_in "Type", with: @eeg_file.type
    click_on "Update Eeg file"

    assert_text "Eeg file was successfully updated"
    click_on "Back"
  end

  test "destroying a Eeg file" do
    visit eeg_files_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Eeg file was successfully destroyed"
  end
end
