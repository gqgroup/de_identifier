desc "Move UH manually-converted EDF to MEDCIS"
task :move_uh_manual_edfs => :environment do
	require 'csv'
	require 'fileutils'
	manual_edf_folder = "/data/slhatoo/Deidentification/UH_EDF"
	# manual_edf_folder = "/data/stao/csr/UH/EEG"
	target_folder = "/data/stao/csr/UH/EEG"
	Dir.foreach(manual_edf_folder) do |entry|
		# NYU0003-YU00YU00000044518701
		if entry == '.' or entry == '..'
			next
		end
		if entry.downcase.ends_with?(".edf")
			source = "#{manual_edf_folder}/#{entry}"
			data_name = entry[0..-5]
			if data_name.ends_with?('EDF')
				data_name = data_name[0..-4]
			end
		end

		record = UhEdfName.where(data_name: data_name).first
		opic_id = record.opic_id if record.present?

		if data_name.present? and opic_id.present?
			opic_ids = ['BSKI0184002156225604','LRAS33316747934601','CUTC0717000376080803','HIVP8604333466677301','IZMA2438000018228901','IZMA2438000018228906','KDXG3264001245287303','MLCL388153342017203','NMOO6903040088117404','NMOO6903040088117406','SJFM7486779532526401','VFUL7006778589672802','VFUL7006778589672803']
			if true
				puts "#{opic_id} - #{data_name}"
				target_EDF_folder = "#{target_folder}/#{opic_id}"
				if File.exists? target_EDF_folder
					begin
	   				mv_output = `mv #{source} #{target_EDF_folder}/`	
		 				if mv_output
		 					puts "Successfully moved edf+ files"
		 				else
		 					puts "Failed to move edf+ files"
		 				end
					rescue => e
						puts e.full_messsages.to_sentence
					end
				else
					begin
						output = `mkdir #{target_EDF_folder}`
						if output
	     				puts "Successfully created folder: #{target_EDF_folder}"
	     				mv_output = `mv #{source} #{target_EDF_folder}/`	
			 				if mv_output
			 					puts "Successfully moved edf+ files"
			 				else
			 					puts "Failed to move edf+ files"
			 				end
	     			else
	     				puts "Failed to create EDF+ folder!"
	     			end
					rescue => e
						puts e.full_messsages.to_sentence
					end
				end
			end
		end
	end
end