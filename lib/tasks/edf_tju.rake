desc "Convert JEFFERSON DATA to EDF-Plus"
require 'csv'
require 'fileutils'
namespace :edf_tju do

	# rename .pnt to .PNT
	task :rename_pnt => :environment do
		index = 0
		EegFile.where("file_type=? and site_name=?", 'PNT', 'TJU').where('edf_plus is null').find_in_batches(batch_size: 1000).each do |b|
			puts index+1
			index = index + 1
			b.each do |eeg_file|
				# if eeg_file.path.end_with?('.pnt')
					old_file_path = "#{eeg_file.path[0..-5]}.pnt"
					new_file_path = "#{eeg_file.path[0..-5]}.PNT"
					system("mv \"#{old_file_path}\" \"#{new_file_path}\"")
					eeg_file.path = new_file_path
					eeg_file.save
				# end
			end
		end
	end

	# remove space in filenames
	task :remove_space => :environment do
		index = 0
		folders = []
		EegFile.where("file_type=? and site_name=?", 'EEG', 'TJU').where('edf_plus is null').find_in_batches(batch_size: 1000).each do |b|
			puts index+1
			index = index + 1
			b.each do |eeg_file|
				folder = File.dirname(eeg_file.path)
				if folders.include?folder
					puts "=====done already====="
				else
					folders << folder
					if system("#{Rails.root}/remove_space.sh #{folder}")
						puts "Success"
						eeg_file.path = eeg_file.path.gsub(" ", "_")
						eeg_file.save
					else
						puts "Failed to remove spaces"
					end
				end

			end
		end
	end

	task :edf_tju => :environment do
		index = 0
		EegFile.where("file_type=? and site_name=?", 'EEG', 'TJU').where('edf_plus is null').find_in_batches(batch_size: 1000).each do |b|
			puts index+1
			index = index + 1
			b.each do |eeg_file|
				if eeg_file.path.include?('(2)')
					next
				else
					begin
						output = system("nk2edf #{eeg_file.path}")
						if output
	     				puts "Success."
	     				eeg_file.edf_plus = true
	     				eeg_file.save
	     			else
	     				puts "Failed!"
	     				eeg_file.edf_plus = false
	     				eeg_file.save
	     			end
					rescue => e
						puts e.full_messsages.to_sentence
					end
				end
			end
		end
	end

	task :move_edf => :environment do
		index = 0
		folders = []
		EegFile.where("file_type=? and site_name=? and edf_plus=?", 'EEG', 'TJU', true).find_in_batches(batch_size: 1000).each do |b|
			puts index+1
			index = index + 1
			b.each do |eeg_file|
				# path: "/data/slhatoo/Deidentification/IOWA/UI-097-DOLY2565209897704401/NKT/EEG2100/a.EEG"
				# eeg2100_folder = "/data/slhatoo/Deidentification/IOWA/UI-097-DOLY2565209897704401/NKT/EEG2100"
				eeg2100_folder = File.dirname(eeg_file.path)
				# nkt_folder = "/data/slhatoo/Deidentification/IOWA/UI-097-DOLY2565209897704401/NKT"
				nkt_folder = File.dirname(eeg2100_folder)
				# opic_id_folder = "/data/slhatoo/Deidentification/IOWA/UI-097-DOLY2565209897704401"
				opic_id_folder = File.dirname(nkt_folder)
				if opic_id_folder.present?

					if folders.include? opic_id_folder
						puts "=====done already====="
					else
						folders << opic_id_folder
						begin
							output = `mkdir #{opic_id_folder}/EDF+`
							if output
		     				puts "Successfully created EDF+ folder."
		     				mv_output = `mv #{eeg2100_folder}/*.edf #{opic_id_folder}/EDF+/`	
		     				if mv_output
		     					puts "Successfully moved edf+ files"
		     				else
		     					puts "Failed to move edf+ files"
		     				end
		     			else
		     				puts "Failed to create EDF+ folder!"
		     				eeg_file.edf_plus = false
		     				eeg_file.save
		     			end
						rescue => e
							puts e.full_messsages.to_sentence
						end
					end
				end
			end
		end
	end

end


# desc "find identifier embedded in EEG files"
# # Exception List
# # /data/slhatoo/Jackie/NKT/EEG2100/DA008AE7.EEG
# # /data/slhatoo/Jackie/NKT/EEG2100/DA008AE6.EEG
# require 'fileutils'
# require 'find'
# namespace :de_identifier do
	# CONSTANTS
	# MRN_OFFSET=48
	# MRN_SIZE=12
	
	# DATE_OFFSET=64
	# DATE_SIZE=8
	
	# NAME_OFFSET=79
	# NAME_SIZE=20

	# MRN_PNT_OFFSET=1540
	# NAME_PNT_OFFSET=1582
	# NAME_PTN_OFFSET=9907
	
# task :backfill_eeg_files => :environment do
# 		EegFile.where(file_type: 'EEG', name: nil).each do |eeg_file|
# 			path = eeg_file.path
# 			pnt_path = "#{path[0..-4]}PNT"
# 			pnt_file = EegFile.where(path: pnt_path).first
# 			if pnt_file.present?
# 				eeg_file.name = pnt_file.name
# 				eeg_file.save
# 			else
# 				puts "No PNT file found #{pnt_path}"
# 			end
# 		end
# 	end

# 	task :find_identifier => :environment do
# 		# /EEG2100/
# 		eeg_path = ENV['path']
# 		if eeg_path.present?
# 			traverse(eeg_path, 0)
# 		else
# 			# Put paths here to traverse
# 			eeg_paths = [
				
# 			]
# 			eeg_paths.each do |eeg_path|
# 				traverse(eeg_path, 0)
# 			end
# 		end
# 	end


# 	task :show_stats => :environment do
# 		eeg_files = EegFile.where(file_type: 'EEG')
# 		puts "Currently we have found #{eeg_files.size} EEG files under Jakie"
# 		opic_ids = CoversionList.all.collect{|l| l.opic_id}
# 		to_do = eeg_files.select{|f| opic_ids.include?(f.name) }
# 		puts "#{to_do.size} EEG files are on the TODO list"
		
# 	end

# 	task :convert_eeg => :environment do
# 		eeg_files = EegFile.where(file_type: 'EEG').where('edf_plus is null')
# 		opic_ids = CoversionList.all.collect{|l| l.opic_id}
# 		# opic_ids = opic_ids - ["CIND1700000074329001","RANG2601000052727801"]
# 		# opic_ids = ["CIND1700000074329001","RANG2601000052727801"]
		
# 		to_do = eeg_files.select{|f| opic_ids.include?(f.name)}
# 		# first_batch = to_do.first(50)
# 		puts "Convert EEG to EDF+"
# 		to_do.each_with_index do |eeg_file, index|
# 			puts "Converting EEG file #{index}..."
# 			output = system("nk2edf #{eeg_file.path}")
# 			if output
# 				puts "Success."
# 				eeg_file.edf_plus = true
# 				eeg_file.save
# 			else
# 				puts "Failed!"
# 				eeg_file.edf_plus = false
# 				eeg_file.save
# 			end
# 		end
# 	end

# 	task :move_edf => :environment do
# 		eeg_files = EegFile.where(file_type: 'EEG', edf_plus: true)
# 		puts "Move EDF+ files"
# 		system("mv /data/slhatoo/Jackie/NKT/EEG2100/*.edf /data/slhatoo/Jackie_EDF")
# 	end

# 	def print_current_eeg
# 		eeg_files = EegFile.where(file_type: 'EEG')
# 		puts "Currently we have found #{eeg_files.size}"
# 	end

# 	def traverse(file_path, total_size)

# 		postfixes = ["BFT", "CMT", "CN2", "EEG", "LOG", "PNT", "PTN", 'EGF']
# 		pattern = "*.{#{postfixes.join(',')}}"
# 		# puts pattern
# 		puts "Traversing #{file_path}"
# 		size = 0
# 		if File.directory?(file_path)
# 			# target_files = File.join(file_path, pattern)
# 			Dir.foreach(file_path) do |entry|
# 				if File.directory?("#{file_path}#{entry}")
# 					next
# 				end
# 				file_ex = entry[-4..-1]
# 				if postfixes.collect{|e| ".#{e}"}.include?(file_ex)
# 					total_size += 1
# 					entry_path = "#{file_path}#{entry}"
# 					eeg_file = EegFile.new(path: entry_path)
# 					mrn = IO.binread(entry_path, MRN_SIZE, MRN_OFFSET).gsub(/\x00/, '')
# 					date = IO.binread(entry_path, DATE_SIZE, DATE_OFFSET).gsub(/\x00/, '')
# 					name = IO.binread(entry_path, NAME_SIZE, NAME_OFFSET).gsub(/\x00/, '')
# 					eeg_file.file_type = entry[-3..-1]
# 					if name.present?
# 						eeg_file.name = name
# 					else
# 						if file_ex.downcase == '.pnt'
# 							pnt_name_offset = 1582
# 							name = IO.binread(entry_path, NAME_SIZE, pnt_name_offset).gsub(/\x00/, '')
# 							eeg_file.name = name
# 						end
# 					end
					
# 					eeg_file.mrn = mrn
# 					eeg_file.date = date
# 					begin
# 						eeg_file.save	
# 					rescue
# 						puts name
# 						puts date
# 						puts mrn
# 						puts entry_path
# 					end
# 	      end

# 			end
# 			puts size
# 			puts total_size
# 		end
# 	end

# end
