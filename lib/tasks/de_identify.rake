desc "Deidentify Files"

require 'csv'
require 'fileutils'
task :de_identify => :environment do

	index = 0
	EegFile.where("de_identified is NULL and file_type!=?", 'PTN').find_in_batches(batch_size: 1000).each do |b|
		puts index+1
		index = index + 1
		b.each do |eeg_file|
			eeg_file.de_identify
		end
	end
end


