desc "Convert JEFFERSON DATA to EDF-Plus"
require 'csv'
require 'fileutils'
namespace :edf_uiowa do

	task :rename_pnt => :environment do
		index = 0
		EegFile.where("file_type=? and site_name=?", 'PNT', 'UIOWA').where('edf_plus is null or edf_plus=0').find_in_batches(batch_size: 1000).each do |b|
			puts index+1
			index = index + 1
			b.each do |eeg_file|
				# if eeg_file.path.end_with?('.pnt')
					old_file_path = "#{eeg_file.path[0..-5]}.pnt"
					new_file_path = "#{eeg_file.path[0..-5]}.PNT"
					system("mv \"#{old_file_path}\" \"#{new_file_path}\"")
					eeg_file.path = new_file_path
					eeg_file.save
				# end
			end
		end
	end

	task :edf_uiowa => :environment do
		index = 0
		EegFile.where("file_type=? and site_name=?", 'EEG', 'UIOWA').where('edf_plus is null or edf_plus =?', false).find_in_batches(batch_size: 1000).each do |b|
			puts index+1
			index = index + 1
			b.each do |eeg_file|
				if eeg_file.path.include?('(2)')
					next
				else
					begin
						output = system("nk2edf \"#{eeg_file.path}\"")
						if output
	     				puts "Success."
	     				eeg_file.edf_plus = true
	     				eeg_file.save
	     			else
							puts eeg_file.path
	     				puts "Failed!"
	     				eeg_file.edf_plus = false
	     				eeg_file.save
	     			end
					rescue => e
						puts e.full_messsages.to_sentence
					end
				end
			end
		end
	end

	task :move_edf => :environment do
		index = 0
		folders = []
		EegFile.where("file_type=? and site_name=? and edf_plus=?", 'EEG', 'UIOWA', true).find_in_batches(batch_size: 1000).each do |b|
			puts index+1
			index = index + 1
			b.each do |eeg_file|
				# path: "/data/slhatoo/Deidentification/IOWA/UI-097-DOLY2565209897704401/NKT/EEG2100/a.EEG"
				# eeg2100_folder = "/data/slhatoo/Deidentification/IOWA/UI-097-DOLY2565209897704401/NKT/EEG2100"
				eeg2100_folder = File.dirname(eeg_file.path)
				# nkt_folder = "/data/slhatoo/Deidentification/IOWA/UI-097-DOLY2565209897704401/NKT"
				nkt_folder = File.dirname(eeg2100_folder)
				# opic_id_folder = "/data/slhatoo/Deidentification/IOWA/UI-097-DOLY2565209897704401"
				opic_id_folder = "/data/stao/csr/UIowa/NEW_EEG/#{eeg_file.opic_id}"
				if opic_id_folder.present?

					if folders.include? opic_id_folder
						puts "=====done already====="
					else
						folders << opic_id_folder
						begin
							output = `mkdir #{opic_id_folder}`
							if output
		     				# puts "Successfully created EDF+ folder."
		     				mv_output = `mv #{eeg2100_folder.gsub(' ', '\ ')}/*.edf #{opic_id_folder}/`	
		     				if mv_output
		     					puts "Successfully moved edf+ files"
		     				else
		     					puts "Failed to move edf+ files"
		     				end
		     			else
		     				puts "Failed to create EDF+ folder!"
		     				eeg_file.edf_plus = false
		     				eeg_file.save
		     			end
						rescue => e
							puts e.full_messsages.to_sentence
						end
					end
				end
			end
		end
	end

end


