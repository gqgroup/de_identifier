desc "Jetli: X => N; Jackie: Y => M"
task :map_disk_number => :environment do
	EegFile.all.each do |file|
		path = file.path
		if path.start_with?("X:")
			file.path = path.gsub('X:', 'N:')
			file.save
		elsif path.start_with?("Y:")
			file.path = path.gsub('Y:', 'M:')
			file.save
		else
			puts file.path
		end 
	end
end