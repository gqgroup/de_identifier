desc "Write new edf headers into txt files"
task :write_new_headers => :environment do
	total_new_headers = 0
	base_path = "/data/stao/csr"
	folders = ["UH/NEW_EEG", "UIowa/NEW_EEG", "Jefferson/NEW_EEG", "Northwestern/NEW_EEG"]
	folders.each do |folder|
		Dir.foreach("#{base_path}/#{folder}") do |entry|
			unless ['.', '..'].include? entry
				Dir.foreach("#{base_path}/#{folder}/#{entry}").each do |header_entry|
					if header_entry.end_with?(".txt")
						file = "#{base_path}/#{folder}/#{entry}/#{header_entry}"
						header = `head -n1 #{file}`
						start_index = header.index("Startdate")
						end_index = header.index("EDF+")
						if end_index.nil?
							puts "#{file} has no EDF+ in it"
						else
							end_index = end_index - 24
							sub_header = header[start_index..(end_index - 1)]
							
							if sub_header.size > 80
								total_new_headers = total_new_headers + 1
								sub_header = sub_header[0..79]
								new_header = "#{header[0..(start_index-1)]}#{sub_header}#{header[end_index..255]}"
								new_header.rjust(256, " ")
								begin
									f = File.open("#{file[0..-5]}_new.txt", 'w')
									f.puts new_header
									f.close
								rescue => e
									puts e.full_messages.to_sentence
								end
							end

							
						end
						
					end
				end
			end
		end
	end
end