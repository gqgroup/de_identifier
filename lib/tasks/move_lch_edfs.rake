desc "Move NYU manually-converted EDF to MEDCIS"
task :move_lch_edfs => :environment do
	require 'csv'
	require 'fileutils'
	manual_edf_folder = "/data/slhatoo/Deidentification/LCH_EDF"
	target_folder = "/data/stao/csr/Northwestern/LCH_EEG"
	Dir.foreach(manual_edf_folder) do |entry|
		# NYU0003-YU00YU00000044518701
		if entry == '.' or entry == '..'
			next
		end
		source_folder = "#{manual_edf_folder}/#{entry}/"
		# if false
			begin
 				mv_output = `mv #{source_folder} #{target_folder}/`	
 				if mv_output
 					puts "Successfully moved edf+ files"
 				else
 					puts "Failed to move edf+ files"
 				end
			rescue => e
				puts e.full_messsages.to_sentence
			end
		# end
	end
end