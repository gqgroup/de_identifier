desc "Deidentify JEFFERSON DATA"
require 'csv'
require 'fileutils'
task :deid_uiowa => :environment do
	index = 0
	EegFile.where("de_identified is NULL and file_type!=? and site_name=?", 'PTN', 'UIOWA').find_in_batches(batch_size: 1000).each do |b|
		puts index+1
		index = index + 1
		b.each do |eeg_file|
			eeg_file.de_identify_uiowa
		end
	end
end
