task :import_patients => :environment do

	file_name = ENV['file']
	site_name = ENV['site']

	CSV.foreach("#{Rails.root}/#{file_name}", headers: true) do |row|
		# UH0578	NAJO5701000025019803	1084057	John	Mihalina 3/9/13	3/11/13 2/10/13
		local_patient_number = row[0]
		opic_id = row[1]
		mrn = row[2]
		from_date = row[5]
		from_date = Date.parse(from_date) if from_date.present?
		to_date = row[6]
		puts to_date
		to_date = Date.parse(to_date) if to_date.present?
		first_name = row[3]
		last_name = row[4]
		# dob
		dob = row[7]
		dob = Date.parse(dob) if dob.present?
		Patient.create(local_patient_number: local_patient_number, opic_id: opic_id, mrn: mrn, doa: from_date, dod: to_date, dob: dob, first_name:first_name, last_name: last_name, site_name: site_name)
	end


	
end
