desc "Write edf headers into txt files"
task :write_headers => :environment do
	base_path = "/data/stao/csr"
	folders = ["UH/NEW_EEG", "UIowa/NEW_EEG", "Northwestern/NEW_EEG"]
	# folders = ["Jefferson/NEW_EEG"]
	folders.each do |folder|
		Dir.foreach("#{base_path}/#{folder}") do |entry|
			unless ['.', '..'].include? entry
				Dir.foreach("#{base_path}/#{folder}/#{entry}").each do |edf_entry|
					if edf_entry.end_with?(".edf")
						file = "#{base_path}/#{folder}/#{entry}/#{edf_entry}"
						first_line = `head -n1 #{file}`
						header = first_line[0..255]
						begin
							f = File.open("#{file[0..-5]}.txt", 'w')
							f.puts header
							f.close
						rescue => e
							puts e.full_messages.to_sentence
						end
					end
				end
			end
		end
	end
end