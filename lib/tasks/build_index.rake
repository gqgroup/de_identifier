desc "find identifier embedded in EEG files"

require 'fileutils'
require 'find'
namespace :eeg_index do
	# CONSTANTS
	MRN_OFFSET=48
	MRN_SIZE=12
	
	DATE_OFFSET=64
	DATE_SIZE=8
	
	NAME_OFFSET=79
	NAME_SIZE=20

	MRN_PNT_OFFSET=1540
	NAME_PNT_OFFSET=1582
	NAME_PTN_OFFSET=9907

	desc "Update EEG OPIC for UH EEG Files"
	task :update_uh_opic_ids => :environment do
		EegFile.where(site_name: 'UH', file_type: 'EEG').each do |eeg_file|
			if eeg_file.opic_id.present?
				eeg_file.opic_id = eeg_file.opic_id.gsub(' ', '')
				eeg_file.save
			elsif eeg_file.name.present?
				eeg_file.opic_id = eeg_file.name
				eeg_file.save
			else
				eeg_file_path = eeg_file.path
				pnt_file_path = "#{eeg_file_path[0..-5]}.PNT"
				pnt_file = EegFile.where(path: pnt_file_path).first
				if pnt_file.present?
					if pnt_file.name.present?
						eeg_file.opic_id = pnt_file.name
						eeg_file.save
					else
						puts "No OPIC id found for EEG File: #{eeg_file_path}"
					end
				else
						puts "No OPIC id found for EEG File: #{eeg_file_path}"
				end
			end
		end
	end
	
	task :truncate_index => :environment do
		table_name = 'eeg_files'
		ActiveRecord::Base.connection.execute("TRUNCATE #{table_name}")
	end

	task :build_index => :environment do
		site_name = ENV['site_name']
		eeg_path = ENV['path']

		if site_name == 'TJU'
			opic_ids = Patient.where(site_name: 'TJU').collect(&:opic_id)
			Dir.foreach(eeg_path) do |entry|
				if opic_ids.include?(entry)
					traverse("#{eeg_path}/#{entry}/NKT/EEG2100/", 0, site_name, entry)
				end
			end
		elsif site_name == 'UIOWA'
			opic_ids = Patient.where(site_name: 'UIOWA').collect(&:opic_id)
			Dir.foreach(eeg_path) do |entry|
				opic_id = entry.split('-').last.strip
				if opic_ids.include?(opic_id)
					Dir.foreach("#{eeg_path}/#{entry}") do |e|
						if e == '.' or e == '..'
							next
						elsif e == 'NKT'
							traverse("#{eeg_path}/#{entry}/NKT/EEG2100/", 0, site_name, opic_id)
						else
							traverse("#{eeg_path}/#{entry}/#{e}/NKT/EEG2100/", 0, site_name, opic_id)
						end
					end
				else
					puts "======"
					puts "======"
					puts "======"
					puts "#{entry} failed to find match in UIOWA patient list"
					puts "======"
					puts "======"
					puts "======"
				end
			end
		
		elsif site_name == 'NW'
			opic_ids = Patient.where(site_name: 'NW').collect(&:opic_id)
			Dir.foreach(eeg_path) do |entry|
				opic_id = entry.split('-').last.strip
				if opic_ids.include?(opic_id)
					traverse("#{eeg_path}/#{entry}/NKT/EEG2100/", 0, site_name, opic_id)
				else
					puts "======"
					puts "======"
					puts "======"
					puts "#{entry} failed to find match in NW patient list"
					puts "======"
					puts "======"
					puts "======"
				end
			end
		elsif site_name == 'NW-PRISM'
			opic_ids = Patient.where(site_name: 'NW-PRISM').collect(&:opic_id)
			Dir.foreach(eeg_path) do |entry|
				if entry == '.' or entry == '..'
					next
				end
				opic_id = entry.split('-')[-2].strip
				if opic_ids.include?(opic_id)
					traverse("#{eeg_path}/#{entry}/NKT/EEG2100/", 0, site_name, opic_id)
				else
					puts "======"
					puts "======"
					puts "======"
					puts "#{entry} failed to find match in NW-PRISM patient list"
					puts "======"
					puts "======"
					puts "======"
				end
			end
		elsif site_name == 'LCH'
			opic_ids = Patient.where(site_name: 'LCH').collect(&:opic_id)
			Dir.foreach(eeg_path) do |entry|
				opic_id = entry.strip
				if opic_ids.include?(opic_id)
					traverse("#{eeg_path}/#{entry}/NKT/EEG2100/", 0, site_name, opic_id)
				else
					puts "======"
					puts "======"
					puts "======"
					puts "#{entry} failed to find match in LCH patient list"
					puts "======"
					puts "======"
					puts "======"
				end
			end
		elsif site_name == 'UH'
			traverse("#{eeg_path}/NKT/EEG2100/", 0, site_name)
		elsif site_name == 'UCLA'
			# 022Y2TBK900099419101_6022 folder name
			Dir.foreach(eeg_path) do |entry|
				opic_id = entry.split('_').first.strip
				Dir.foreach("#{eeg_path}/#{entry}") do |e|
					if e == '.' or e == '..'
						next
					elsif e == 'NKT'
						traverse("#{eeg_path}/#{entry}/NKT/EEG2100/", 0, site_name, opic_id)
					end
				end
			end
		end


	end

	def traverse(file_path, total_size, site_name, opic_id=nil)
		puts "Traversing #{file_path}"
		size = 0
		current = Time.now
		if File.directory?(file_path)
			# target_files = File.join(file_path, pattern)
			Dir.foreach(file_path) do |entry|
				if entry == '.' or entry == '..'
					next
				end
				if File.directory?("#{file_path}#{entry}")
					next
				else
					file_ex = entry.split('.').last
					if site_name == 'UH'
						if ["EEG", "LOG", "PNT"].include? file_ex.upcase
							record(file_path, entry, site_name, opic_id)
							size = size + 1
							if size%1000==0
								puts "============================="
								puts "============================="
								puts "#{Time.now - current} seconds"
								current = Time.now
								puts "============================="
								puts "============================="
							end
						end
					else
						if ["BFT", "CMT", "CN2", "EEG", "LOG", "PNT", "PTN", 'EGF'].include? file_ex.upcase
							record(file_path, entry, site_name, opic_id)
							size = size + 1
							if size%1000==0
								puts "============================="
								puts "============================="
								puts "#{Time.now - current} seconds"
								current = Time.now
								puts "============================="
								puts "============================="
							end
						end
					end
		    end
			end
			puts size
		else

		end
	end

	def record(file_path, entry, site_name, opic_id=nil)
		puts entry
		postfixes = ["BFT", "CMT", "CN2", "EEG", "LOG", "PNT", "PTN", 'EGF']
		file_ex = entry[-3..-1]
		entry_path = "#{file_path}#{entry}"
		eeg_file = EegFile.new(site_name: site_name, path: entry_path, file_type: entry[-3..-1], opic_id: opic_id)
		mrn = nil
		date = nil
		name = nil
		if file_ex.downcase == 'egf'
			f = File.open(entry_path, 'rb')
			lines = f.readlines
			mrn = lines.first.gsub('ID=', '').strip
			name = lines[2].gsub("PATNAME=", '').strip
			date = lines[13].gsub("EEGDATE=", '').gsub('/', '').strip
		else
			unless entry.start_with?('Pattern_')
				mrn = IO.binread(entry_path, MRN_SIZE, MRN_OFFSET).gsub(/\x00/, '')
				date = IO.binread(entry_path, DATE_SIZE, DATE_OFFSET).gsub(/\x00/, '')
				name = IO.binread(entry_path, NAME_SIZE, NAME_OFFSET).gsub(/\x00/, '')
				if name.present?
					eeg_file.name = name
				else
					if file_ex.downcase == 'pnt'
						pnt_name_offset = 1582
						name = IO.binread(entry_path, NAME_SIZE, pnt_name_offset).gsub(/\x00/, '')
					end
				end
			end
		end
		begin 
			eeg_file.mrn = mrn
			eeg_file.name = name
			eeg_file.date = date
			eeg_file.save	
		rescue
			eeg_file.mrn = ""
			eeg_file.name = ""
			eeg_file.date = ""
			eeg_file.save
		end
	end

	def traverse_jakie(file_path, total_size)

		puts "Traversing #{file_path}"
		size = 0
		if File.directory?(file_path)
			# target_files = File.join(file_path, pattern)
			Dir.foreach(file_path) do |entry|
				if entry == '.' or entry == '..'
					next
				end
				if File.directory?("#{file_path}#{entry}")
					next
					# if entry.end_with?('PTN')
					# 	inner_path = "#{file_path}#{entry}/"
					# 	Dir.foreach(inner_path) do |inner_entry|
					# 		if inner_entry =='.' or inner_entry =='..'
					# 			next
					# 		end
					# 		record_jakie(inner_path, inner_entry)
					# 		size = size + 1
					# 	end
					# end
				else
					file_ex = entry[-3..-1]
					if ["BFT", "CMT", "CN2", "EEG", "LOG", "PNT", "PTN", 'EGF'].include? file_ex
						record_jakie(file_path, entry)
						size = size +1
						if size % 1000 == 1
							puts size
						end
					end
		    end

			end
			puts size
		end
	end

	def record_jakie(file_path, entry)
		postfixes = ["BFT", "CMT", "CN2", "EEG", "LOG", "PNT", "PTN", 'EGF']
		file_ex = entry[-4..-1]
		entry_path = "#{file_path}#{entry}"
		eeg_file = JakieEegFile.new(path: entry_path, file_type: entry[-3..-1])
	
		if file_ex.downcase == '.egf'
			f = File.open(entry_path, 'rb')
			lines = f.readlines
			mrn = lines.first.gsub('ID=', '').strip
			name = lines[2].gsub("PATNAME=", '').strip
			date = lines[13].gsub("EEGDATE=", '').gsub('/', '').strip
		elsif entry.start_with?('Pattern') and entry.end_with?('.PTN')
			eeg_file.save
		else
			mrn = IO.binread(entry_path, MRN_SIZE, MRN_OFFSET).gsub(/\x00/, '')
			date = IO.binread(entry_path, DATE_SIZE, DATE_OFFSET).gsub(/\x00/, '')
			name = IO.binread(entry_path, NAME_SIZE, NAME_OFFSET).gsub(/\x00/, '')
			if name.present?
				eeg_file.name = name
			else
				if file_ex.downcase == '.pnt'
					pnt_name_offset = 1582
					name = IO.binread(entry_path, NAME_SIZE, pnt_name_offset).gsub(/\x00/, '')
					eeg_file.name = name
				end
			end
		end
		eeg_file.name = name
		eeg_file.mrn = mrn
		eeg_file.date = date
		begin
		 eeg_file.save
		rescue => e
			puts e.message
			eeg_file.name = nil
			eeg_file.mrn = nil
			eeg_file.date = nil
			eeg_file.save
		end	
	end
end
