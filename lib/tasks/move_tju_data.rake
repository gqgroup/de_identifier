desc "move TJU files to the new NKT/EEG2100 folder"
task :move_tju_files => :environment do
	base_path = '/data/slhatoo/Deidentification/JEFFERSON'
	Patient.where(site_name: 'TJU').collect(&:opic_id).each do |opic_id|
		source_folder = "#{base_path}/#{opic_id}/NKT/EEG2100"
		target_folder = "#{base_path}/NKT/EEG2100"
		`mv #{source_folder}/*.* #{target_folder}`	
	end
end