desc 'find jakie workload'

task :find_todo => :environment do 
	jakie_files = []
	file_path ='Y:/NKT/EEG2100/'
	index = 0
	Dir.foreach(file_path) do |entry|
		index = index +1
		if index % 10000 ==0
			puts index
		end
		if entry == '.' or entry == '..'
			next
		end
		if File.directory?("#{file_path}#{entry}")
			next
		end
		if ["BFT", "CMT", "CN2", "EEG", "LOG", "PNT", "PTN", 'EGF'].include? entry[-3..-1]
			jakie_files << "#{file_path}#{entry}"
		end
	end

	completed_files = JakieEegFile.all.pluck(:path)
	puts "Completed: #{completed_files.size}"
	todo = jakie_files - completed_files
	f = File.open("#{Rails.root}/lib/assets/jakie_todo.txt", 'w')
	f.puts todo.join(",")
end