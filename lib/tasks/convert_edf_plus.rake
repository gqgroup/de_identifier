desc "Convert EDF+ to EDF and write seprate annotation files"

require 'edfize/edf.rb'
require 'edfize/signal.rb'
include Edfize, Signal

task :convert_edf => :environment do

  worker_pool = Worker.pool(size: 20)
  (Dir.entries("#{Rails.root}/lib/assets/input") - [".", "..", ".DS_Store", "edfs", "edf_files"]).each do |file|
    edf_path = "#{Rails.root}/lib/assets/input/#{file}"
    new_path = "#{Rails.root}/lib/assets/input/edf_files/new#{file}"
    annotation_path = "#{Rails.root}/lib/assets/input/edf_files/new#{file[0..-5]}.txt"
    json_annotation_path = "#{Rails.root}/lib/assets/input/edf_files/new#{file[0..-5]}.json"
    if File.exist? new_path
      next
    end
    worker_pool.convert_edf_plus(edf_path, new_path, annotation_path, json_annotation_path)
  end
end

