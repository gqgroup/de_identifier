desc "Import UH EDF Names"
task :import_uh_edf_names => :environment do
	data_name = false
	data_name_index = -1
	noises = ["TRUE", "FALSE", 'Clipped', 'Protocol Title', 'SURFACE PEDS', 'Video Name']
	postfix = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"]
	find_data_name = nil
	opic_id = nil
	row_number = 0
	CSV.foreach("#{Rails.root}/lib/assets/uh_edf_data_names.csv", headers: false)  do |row| 
    if (row[0].present? && (row[0].starts_with?('UH') || row[0].starts_with?('PR'))) || (row[1].present? && (row[1].starts_with?('UH') || row[1].starts_with?('PR'))) || (row[2].present? and row[2].size > 12 and !row[2].include?(" ") and postfix.include?(row[2][-2..-1]))
    	find_data_name = true
    	data_name_index = -1
    	opic_id = row[2]
    end

    if find_data_name
    	if data_name_index == -1
	    	row.each_with_index do |field, index|
	    		if field == 'Data Name'
	    			data_name_index = index
	    		end
	    	end
	    else
	    	data_name = row[data_name_index] if row[data_name_index].present? and !(noises.include? row[data_name_index]) and !(row[data_name_index].include?('.'))
	    	if data_name.present?	    	
		    	UhEdfName.where(opic_id: opic_id, data_name: data_name).first_or_create
		    end
	    end
    end
  end
end