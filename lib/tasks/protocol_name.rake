desc "Protocol Name"

require 'csv'
require 'fileutils'
task :protocol_name => :environment do
	file_path = "O:\\de_identifier\\DA9730F6.PNT"
	opic_id = "DOPM973984395669303"
	content = IO.binread(file_path)
	first_name_offset = content.index('ProtocolName')
	(0..9).to_a.each do |i|
		t = IO.binread(file_path, 14, first_name_offset+32+i*46)
		s = ['Bonnar', 'Debra']
		if name_detected(s,t)
			puts "#{t}"
			IO.binwrite(file_path, "#{opic_id[0..3]}#{opic_id[-2..-1]}".ljust(14,' '), first_name_offset+32+i*46)
		end
	end

	first_comment_offset = content.index('ProtocolComment')
	(0..9).to_a.each do |i|
		t = IO.binread(file_path, 14, first_comment_offset+32+i*46)
		s = ['Bonnar', 'Debra']
		if name_detected(s,t)
			puts "#{t}"
			IO.binwrite(file_path, "#{opic_id[0..3]}#{opic_id[-2..-1]}".ljust(14,' '), first_comment_offset+32+i*46)
		end
	end
end

def name_detected(name_tokens, target_text)
  # s = "#{Firstname}#{Lastname}"

	t = target_text.gsub(/\x00/, '')
	t = t.gsub(',', '').downcase
	t = t.gsub(' ', '')
	tokens = name_tokens.collect { |e| e.downcase  }
	tokens.each do |token|
		if t.include?(token)
			return true
		end
	end

	if t.include?(tokens.join) or t.include?(tokens.reverse.join) or t.include?(tokens.collect{|e| e[0]}.join) or t.include?(tokens.reverse.collect{|e| e[0]}.join)
		return true
	else 
		return false
	end
end
