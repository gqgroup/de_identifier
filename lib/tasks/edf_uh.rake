desc "Convert NW DATA to EDF-Plus"
require 'csv'
require 'fileutils'
task :edf_uh => :environment do
	# puts "rename pnt to PNT"
	# index = 0
	# EegFile.where("file_type=? and site_name=?", 'PNT', 'NW').where('edf_plus is null or edf_plus=0').find_in_batches(batch_size: 1000).each do |b|
	# 	puts index+1
	# 	index = index + 1
	# 	b.each do |eeg_file|
	# 		# if eeg_file.path.end_with?('.pnt')
	# 			old_file_path = "#{eeg_file.path[0..-5]}.pnt"
	# 			new_file_path = "#{eeg_file.path[0..-5]}.PNT"
	# 			system("mv \"#{old_file_path}\" \"#{new_file_path}\"")
	# 			eeg_file.path = new_file_path
	# 			eeg_file.save
	# 		# end
	# 	end
	# end

	index = 0
	EegFile.where("file_type=? and site_name=? and opic_id is not NULL", 'EEG', 'UH').where('edf_plus is null or edf_plus =?', false).find_in_batches(batch_size: 1000).each do |b|
		puts index+1
		index = index + 1
		b.each do |eeg_file|
			if eeg_file.path.include?('(2)')
				next
			else
				begin

					output = system("nk2edf \"#{eeg_file.path}\"")
					if output
     				puts "Success."
     				eeg_file.edf_plus = true
     				eeg_file.save
     				eeg2100_folder = File.dirname(eeg_file.path)
						opic_id_folder = "/data/stao/csr/UH/NEW_EEG/#{eeg_file.opic_id}"
						begin
							output = `mkdir #{opic_id_folder}`
							if output
		     				# puts "Successfully created EDF+ folder."
		     				mv_output = `mv #{eeg2100_folder.gsub(' ', '\ ')}/*.edf #{opic_id_folder}/`	
		     				if mv_output
		     					puts "Successfully moved edf+ files"
		     				else
		     					puts eeg_file.opic_id
		     					puts "Failed to move edf+ files"
		     				end
		     			else
		     				puts "Failed to create EDF+ folder!"
		     				eeg_file.edf_plus = false
		     				eeg_file.save
		     			end
						rescue => e
							puts e.full_messsages.to_sentence
						end
     			else
						puts eeg_file.path
     				puts "Failed!"
     				eeg_file.edf_plus = false
     				eeg_file.save
     			end
				rescue => e
					puts e.full_messsages.to_sentence
				end
			end
		end
	end

	
end


