require 'celluloid/current'
require 'edfize/edf.rb'
require 'edfize/signal.rb'

class Worker
  include Edfize, Signal, ActionView::Helpers::NumberHelper
  
  def convert_edf_plus(edf_path, new_path, annotation_path, json_annotation_path)
  	edf = Edfize::Edf.new(edf_path)
  	# Loads the data section of the EDF into Signal objects
    edf.load_signals
    puts "#{number_to_human_size(File.size(edf_path))}"
    puts "#{Time.now}"

    start = Time.now
    IO.binwrite(new_path, edf.version)
    current_offset = 0
    
    
    # Wirte edf header
    current_offset = write_header_section(edf, new_path, current_offset)
    
    # Write singal header, n-1 sinals by removing the annotation signal 
    current_offset = write_signal_header(edf, new_path, current_offset)
    # puts "Write Header: #{Time.now - start} seconds"
    # Write signals
    # Here is the error starts
    
    start = Time.now
    write_signal_digital_values(edf, new_path, current_offset)
    # puts "Write Signals: #{Time.now - start} seconds"

    # Extract EEG Annotations
    start = Time.now
    write_eeg_annotations(edf, edf_path, annotation_path)
    # puts "Write annotation txt: #{Time.now - start} seconds"

    start = Time.now
    # Write EEG Annotation in JSON format
    write_json_annotation(annotation_path, json_annotation_path, new_path.split('/').last)
    # puts "Write annotation json: #{Time.now - start} seconds"
  end
	def write_header_section(edf, file, current_offset)
  ns = edf.number_of_signals
  Edfize::Edf::HEADER_CONFIG.keys.each do |section|
    size = Edfize::Edf::HEADER_CONFIG[section][:size]
    if section.to_s == 'reserved'
      section_content = "EDF".ljust(size, ' ')
    elsif section.to_s == 'number_of_signals'
      section_content = (ns-1).to_s.ljust(size, ' ')
    elsif section.to_s == 'number_of_bytes_in_header'
      section_content = ((edf.send(section).to_i)-256).to_s.ljust(size, ' ')
    else
      section_content = edf.send(section).to_s.ljust(size, ' ')
    end
    write_content(file, section_content, current_offset)
    current_offset += size
  end
  return current_offset
end

def write_content(file, section_content, offset)
  IO.binwrite(file, section_content, offset)
end

def write_signal_header(edf, file, current_offset)

  Edfize::Signal::SIGNAL_CONFIG.keys.each do |section|
    current_offset = write_signal_header_section(edf, file, section, current_offset)
  end
  current_offset
end

# EDF signal headers are section by section
def write_signal_header_section(edf, file, section, current_offset)
  channel_mapping = {
    "EEG Fp1-REF" =>  "Fp1",
    "EEG Fp2-REF" =>  "Fp2",
    "EEG F3-REF" => "F3",
    "EEG F4-REF" => "F4",
    "EEG C3-REF" => "C3",
    "EEG C4-REF" => "C4",
    "EEG P3-REF" => "P3",
    "EEG P4-REF" => "P4",
    "EEG O1-REF" => "O1",
    "EEG O2-REF" => "O2",
    "EEG F7-REF" => "F7",
    "EEG F8-REF" => "F8",
    "EEG T7-REF" => "T7",
    "EEG T8-REF" => "T8",
    "EEG P7-REF" => "P7",
    "EEG P8-REF" => "P8",
    "EEG Cz-REF" => "Cz",
    "EEG Fz-REF" => "Fz",
    "EEG Pz-REF" => "Pz",
    "EEG F9-REF" => "F9",
    "EEG F10-REF" =>  "F10",
    "EEG T9-REF" => "T9",
    "EEG T10-REF" =>  "T10",
    "EEG P9-REF" => "P9",
    "EEG P10-REF" =>  "P10",
    "EEG SP1-REF" =>  "SP1",
    "EEG SP2-REF" =>  "SP2",
    "EEG ECG EKG-REF" =>  "EKG",
    "EEG EMG SCM1-REF" => "SCM1",
    "EEG EMG SCM2-REF" => "SCM2",
    "EEG EMG D1-REF" => "D1",
    "EEG EMG D2-REF" => "D2",
    "EEG EMG BB1-REF" =>  "BB1",
    "EEG EMG BB2-REF" =>  "BB2",
    "EEG EMG TB1-REF" =>  "TB1",
    "EEG EMG TB2-REF" =>  "TB2",
    "EEG EMG QF1-REF" =>  "QF1",
    "EEG EMG QF2-REF" =>  "QF2",
    "EEG EMG BF1-REF" =>  "BF1",
    "EEG EMG BF2-REF" =>  "BF2",
    "EEG EMG TA1-REF" =>  "TA1",
    "EEG EMG TA2-REF" =>  "TA2",
    "EDF Annotations" => "EDF Annotations",
    "EEG Fp1-F7" => "Fp1-F7"
  }
  section_size = Edfize::Signal::SIGNAL_CONFIG[section][:size]
  ns = edf.number_of_signals
  edf.signals.each_with_index do |signal, signal_number|
    if section.to_s == 'label'
      # signal_label = channel_mapping[signal.send(section).to_s]
      signal_label ||= signal.send(section).to_s
      # signal_label = signal_label.delete("EEG ")
      content = signal_label.ljust(section_size, ' ')
    else
      content = signal.send(section).to_s.ljust(section_size, ' ')
    end
    write_content(file, content, current_offset+(signal_number*section_size)) 
  end
  current_offset = (ns-1)*section_size + current_offset
  current_offset
end

# EDF sinals are record by record, each record consists of one duration recrod of all signals concatination 

def write_signal_digital_values(edf, file, current_offset)

  all_signals = []
  signal_indexes = []
  ns = edf.number_of_signals
  (0..(ns-2)).to_a.each do |i|
    signal_indexes << 0
  end
  # puts "number of signals: #{ns}"
  # puts "Number of data records: #{edf.number_of_data_records}"
  # puts signal_indexes
  
  (0..(edf.number_of_data_records-1)).to_a.each do |record_index|
    # every data record
    edf.signals.select{|s| not (s.label.include?('EDF Annotation'))}.each_with_index do |signal, index|
      puts "Signal Label: #{signal.label}"
      # puts "Signal samples per data record: #{signal.samples_per_data_record}"
      # puts "Signal total size of digital_values: #{signal.digital_values.size}"
      # each signal index
      samples_per_data_record = signal.samples_per_data_record
      (0..(samples_per_data_record-1)).to_a.each do |i|
        all_signals << signal.digital_values[signal_indexes[index]+i]
      end
      signal_indexes[index] += samples_per_data_record
    end
  end
  write_content(file, all_signals.pack('s<*'), current_offset)
end

def write_eeg_annotations(edf, edf_path, annotation_path)
  s = edf.signals.select{|e| e.label.include? 'EDF Annotation'}.first
  eeg_annotations = []
  (0..(edf.number_of_data_records)).to_a.each do |n|
    # each sample is little endian, 2 bytes
    annotation = IO.binread(edf_path, s.samples_per_data_record*2, compute_offset(edf,n))
    eeg_annotations << annotation
  end
  
  # start_time = Time.parse(get_recording_start_datetime(edf))

  start_time = Time.parse("00:00:00")

  final_annotations = []
  eeg_annotations.each_with_index do |annotation, index|
    if annotation.present?
      annotation = remove_nonprintable_chars(annotation)
      # puts annotation
      # p annotation.chars
      # puts annotation.size
      duration_string = get_duration_since_start(annotation)
      # 2032.879
      seconds = duration_string.split(".").first.to_i if duration_string.present?
      # 2032
      decimal_part = duration_string.split(".").last if (seconds.present? and duration_string.split(".").size > 1)
      # 879
      decimal_part ||= ""
      # puts "Duration String: #{duration_string}"
      # puts "Seconds: #{seconds}"
      # puts "Decimal part: #{decimal_part}"
      pure_annotation = get_stripped_annotation(annotation)
      if pure_annotation.present?
        final_annotations << "#{(start_time+seconds).strftime('%H:%M:%S.')}#{decimal_part[0..2]}  #{pure_annotation}\r\n"
      end
    end
  end
  File.open(annotation_path, 'w+') do |f|
    final_annotations.each do |annotation|
      f.puts (annotation).encode('utf-8', 'binary', invalid: :replace, undef: :replace, replace: '' )
    end
  end
end

def write_json_annotation(annotation_path, json_annotation_path, edf_path)
  anno_json = {}
  anno_json["src_filepath"] = edf_path
  anno_json["src_segment"] = 1
  anno_json["Time"] = []
  anno_json["Text"] = []
  anno_json["warnings"] = ""
  File.open(annotation_path, 'r').each_line do |line|
    if line.present?
      tokens = line.split
      anno_json["Time"] << tokens[0]
      anno_json["Text"] << tokens[1]
    end
  end
  File.open(json_annotation_path, "w+") do |f|
    f.write(anno_json.to_json)
  end

end

def compute_offset(edf, n)
  total_samples_per_data_record = edf.signals.collect(&:samples_per_data_record).inject('+')
  annotation_signal = edf.signals.select{|e| e.label.include? 'EDF Annotation'}.first
  offset = edf.size_of_header + (n+1)*total_samples_per_data_record*2 - annotation_signal.samples_per_data_record*2
  offset
end

def get_recording_start_datetime(edf)
  
  date = edf.start_date_of_recording
  time = edf.start_time_of_recording

  day = date.split.first
  month = date.split[1]
  year = "20" + date.split.last
  # "yyyy-mm-dd"
  date_string = "#{year}-#{month}-#{day}"
  time_string = time.gsub(".", ":")
  
  datetime_string = "#{date_string} #{time_string}"
  datetime_string
end

def get_duration_since_start(annotation)
  # +0.2+2032.879IN BATHROOM
  time_string = get_time_string(annotation)
  # puts time_string
  # +0.2+2032.879
  # p time_string.split("+")
  duration_string = time_string.split("+").last if time_string.split("+").size > 1
  duration_string ||= ""
  # 2032.879
  duration_string

end

def remove_nonprintable_chars(annotation)
  r = annotation.chars.reject{|a| [0,2,8,16,20,21].include?(a.ord)}.join("")
  if r[0] != '+'
    r = r.delete(r[0])
  end
  r
end

def get_time_string(annotation)
  time_info = []
  annotation.chars.each do |char|
    # +0+0.61800Prune
    if ([".", "+"] + (0..9).to_a.collect(&:to_s)).include?(char)
      time_info << char
    else
      break
    end
  end
  time_string = time_info.join("")
  time_string
end

def get_stripped_annotation(annotation)
  anno = annotation.delete(get_time_string(annotation))
  if anno.blank?
    return nil
  end
  anno
end
end
