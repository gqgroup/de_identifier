# == Schema Information
#
# Table name: eeg_files
#
#  id            :bigint(8)        not null, primary key
#  path          :string(255)
#  file_type     :string(255)
#  host          :string(255)
#  name          :string(255)
#  date          :string(255)
#  mrn           :string(255)
#  de_identified :boolean
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  edf_plus      :boolean
#  edf           :boolean
#

class EegFile < ApplicationRecord
	validates_presence_of :path
	validates_uniqueness_of :path

	def self.search_around(name_tokens, from_date, to_date)
		@eeg_files = EegFile.by_name(name_tokens).by_date(from_date, to_date)
    @eeg_files
	end

	def self.by_name(tokens)
		# name format in nihon kohden is lastname, firstname
		# remove possible punctuations 
		tokens = tokens.collect{|t| t.gsub(',', '')}
		tokens = tokens.collect{|t| t.gsub('.', '')}
		# exepected two tokens, at least 1, otherwise it will not reach here
		# 
		if tokens.size > 2
			tokens = [tokens[0], tokens[-1]]
			files = EegFile.where('name in (?)', [tokens.join(", "), tokens.reverse.join(", ")])	
		elsif tokens.size == 2
			files = EegFile.where('name in (?)', [tokens.join(", "), tokens.reverse.join(", ")])	
		elsif tokens.size == 1
			files = EegFile.where('name like ?', "%#{tokens[0]}%")	
		else
			return EegFile.where('id is null')
		end
		files
	end

	def self.by_date(from_date, to_date)
		files = EegFile.where('date >= ? and date <= ?', from_date, to_date)
		files
	end 

	def print
		puts 'hello, world!'
	end

	def with_name?(name_tokens)
		tokens = tokens.collect{|t| t.gsub(',', '')}
		tokens = tokens.collect{|t| t.gsub('.', '')}
		if self.name.blank?
			return false
		else
			name = self.name.downcase
			if tokens.size > 2
				tokens = [tokens[0], tokens[-1]]	
			elsif tokens.size == 2
				tokens = tokens	
			elsif tokens.size == 1
				return name.include?(tokens.first.downcase)	
			else
				return false
			end
			result =  [tokens.join(", "), tokens.reverse.join(", ")].include?(name)
			return result
		end
	end

	def de_identify
		phi_file_types = ["BFT", "CMT", "CN2", "EEG", "LOG", "PNT", 'EGF']
		name_tokens = []
		# file_path = self.path.gsub('NKT', 'NKT3/NKT')
		file_path = self.path
		unless File.exists?(file_path)
			return
		end

		unless phi_file_types.include?(self.path[-3..-1])
			return
		end

		opic_id = nil
		local_patient_number = nil
		first_name = nil
		last_name = nil
		dob = nil
		entry = self.path.split('/').last
		patient = nil
		if self.name.present? or self.mrn.present?
			patient = Patient.find_by_name(self.name, self.date)
			
			if patient.blank?
				patient = Patient.find_by_mrn(self.mrn, self.date)
			end
		else
			data_name = self.path.split('/').last[0..-5]
			same_patient_files = EegFile.where('path like ?', "%#{data_name}.%")
		end

		if patient.present?
			opic_id = "#{patient.opic_id}"
			local_patient_number = patient.local_patient_number
			first_name = patient.first_name
			last_name = patient.last_name
			name_tokens = [first_name, last_name]
			
			if patient.dob.present?
				dob = patient.dob.strftime("%Y%m%d")
			end

			unless File.directory?(path)
				if entry.end_with?('PTN')
					replace_ptn_name(file_path, name_tokens, opic_id)
				elsif phi_file_types.include? entry[-3..-1]
					replace_name(file_path, name_tokens, opic_id)
					replace_mrn(file_path, mrn, local_patient_number)
				elsif entry[-3..-1] == 'EGF'
					de_egf(file_path, local_patient_number, opic_id)
				elsif entry[-3..-1] == 'LOG'
					de_txt(file_path, first_name, last_name, local_patient_number, dob);
				end
				self.de_identified = true
				self.patient_id = patient.id
				self.save
			end	
		end
	end

	def de_identify_tju
		phi_file_types = ["BFT", "CMT", "CN2", "EEG", "LOG", "PNT", 'EGF','pnt']
		name_tokens = []
		# file_path = self.path.gsub('NKT', 'NKT3/NKT')
		file_path = self.path
		unless File.exists?(file_path)
			return
		end

		unless phi_file_types.include?(self.path[-3..-1])
			return
		end

		opic_id = self.opic_id
		patient = Patient.where(opic_id: opic_id).first
		entry = self.path.split('/').last
		if patient.present?
			local_patient_number = patient.local_patient_number
			local_patient_number||=''
			first_name = patient.first_name
			last_name = patient.last_name
			name_tokens = [first_name, last_name]
			
			if patient.dob.present?
				dob = patient.dob.strftime("%Y%m%d")
			end

			unless File.directory?(path)
				if entry.end_with?('PTN')
					replace_ptn_name(file_path, name_tokens, opic_id)
				elsif phi_file_types.include? entry[-3..-1]
					replace_name(file_path, name_tokens, opic_id)
					replace_mrn(file_path, mrn, local_patient_number)
				elsif entry[-3..-1] == 'EGF'
					de_egf(file_path, local_patient_number, opic_id)
				elsif entry[-3..-1] == 'LOG'
					de_txt(file_path, first_name, last_name, local_patient_number, dob);
				end
				self.de_identified = true
				self.patient_id = patient.id
				self.save
			end	
		end
	end

	def de_identify_uiowa
		phi_file_types = ["BFT", "CMT", "CN2", "EEG", "LOG", "PNT", 'EGF','pnt']
		name_tokens = []
		# file_path = self.path.gsub('NKT', 'NKT3/NKT')
		file_path = self.path
		unless File.exists?(file_path)
			return
		end

		unless phi_file_types.include?(self.path[-3..-1])
			return
		end

		opic_id = self.opic_id
		patient = Patient.where(opic_id: opic_id).first
		entry = self.path.split('/').last
		if patient.present?
			local_patient_number = patient.local_patient_number
			local_patient_number||=''
			first_name = patient.first_name
			last_name = patient.last_name
			name_tokens = [first_name, last_name]
			
			if patient.dob.present?
				dob = patient.dob.strftime("%Y%m%d")
			end

			unless File.directory?(path)
				if entry.end_with?('PTN')
					replace_ptn_name(file_path, name_tokens, opic_id)
				elsif phi_file_types.include? entry[-3..-1]
					replace_name(file_path, name_tokens, opic_id)
					replace_mrn(file_path, mrn, local_patient_number)
				elsif entry[-3..-1] == 'EGF'
					de_egf(file_path, local_patient_number, opic_id)
				elsif entry[-3..-1] == 'LOG'
					de_txt(file_path, first_name, last_name, local_patient_number, dob);
				end
				self.de_identified = true
				self.patient_id = patient.id
				self.save
			end	
		end
	end

	def de_identify_nw
		phi_file_types = ["BFT", "CMT", "CN2", "EEG", "LOG", "PNT", 'EGF','pnt']
		name_tokens = []
		# file_path = self.path.gsub('NKT', 'NKT3/NKT')
		file_path = self.path
		unless File.exists?(file_path)
			return
		end

		unless phi_file_types.include?(self.path[-3..-1])
			return
		end

		opic_id = self.opic_id
		patient = Patient.where(opic_id: opic_id).first
		entry = self.path.split('/').last
		if patient.present?
			local_patient_number = patient.local_patient_number
			local_patient_number||=''
			first_name = patient.first_name
			last_name = patient.last_name
			name_tokens = [first_name, last_name]
			
			if patient.dob.present?
				dob = patient.dob.strftime("%Y%m%d")
			end

			unless File.directory?(path)
				if entry.end_with?('PTN')
					replace_ptn_name(file_path, name_tokens, opic_id)
				elsif phi_file_types.include? entry[-3..-1]
					replace_name(file_path, name_tokens, opic_id)
					replace_mrn(file_path, mrn, local_patient_number)
				elsif entry[-3..-1] == 'EGF'
					de_egf(file_path, local_patient_number, opic_id)
				elsif entry[-3..-1] == 'LOG'
					de_txt(file_path, first_name, last_name, local_patient_number, dob);
				end
				self.de_identified = true
				self.patient_id = patient.id
				self.save
			end	
		end
	end

	def de_identify_nw_prism
		phi_file_types = ["BFT", "CMT", "CN2", "EEG", "LOG", "PNT", 'EGF','pnt']
		name_tokens = []
		# file_path = self.path.gsub('NKT', 'NKT3/NKT')
		file_path = self.path
		unless File.exists?(file_path)
			return
		end

		unless phi_file_types.include?(self.path[-3..-1])
			return
		end

		opic_id = self.opic_id
		patient = Patient.where(opic_id: opic_id).first
		entry = self.path.split('/').last
		if patient.present?
			local_patient_number = patient.local_patient_number
			local_patient_number||=''
			first_name = patient.first_name
			last_name = patient.last_name
			name_tokens = [first_name, last_name]
			
			if patient.dob.present?
				dob = patient.dob.strftime("%Y%m%d")
			end

			unless File.directory?(path)
				if entry.end_with?('PTN')
					replace_ptn_name(file_path, name_tokens, opic_id)
				elsif phi_file_types.include? entry[-3..-1]
					replace_name(file_path, name_tokens, opic_id)
					replace_mrn(file_path, mrn, local_patient_number)
				elsif entry[-3..-1] == 'EGF'
					de_egf(file_path, local_patient_number, opic_id)
				elsif entry[-3..-1] == 'LOG'
					de_txt(file_path, first_name, last_name, local_patient_number, dob);
				end
				self.de_identified = true
				self.patient_id = patient.id
				self.save
			end	
		end
	end

	def de_egf(file_path, local_patient_number, opic_id)
		f = File.open(file_path, 'rb')
		content = f.read
		content = content.gsub(/ID=(\w)+\n/, "ID=#{local_patient_number}\n")
		content = content.gsub(/PATNAME=[\w,\s\/]+\n/, "PATNAME=#{opic_id}\n")
		content = content.gsub(/BIRTHD=[\w,\s\/]+\n/, "BIRTHD=\n")
		File.open(file_path, "wb") {|file| file.puts content }
	end

	def replace_name(file_path, name_tokens, opic_id)

		file_format = file_path[-3..-1]	
		name_offset=79
		name_size=20
		content = opic_id.ljust(name_size, ' ')
		IO.binwrite(file_path, content, name_offset)
		name_pnt_offset = 1582
		dob_pnt_offset = 1632
		dob_pnt_size = 10
		dob_de_content='01/01/0001'
		if file_format.upcase == 'PNT'
			IO.binwrite(file_path, content, name_pnt_offset)
			IO.binwrite(file_path, dob_de_content, dob_pnt_offset)
			replace_protocol_comment(file_path, name_tokens, opic_id)
		end
	end

	def replace_protocol_comment(file_path, name_tokens, opic_id)
		content = IO.binread(file_path)
		first_comment_offset = content.index('ProtocolComment')
		first_name_offset = content.index('ProtocolName')
		if first_name_offset.present?
			(0..9).to_a.each do |i|
				t = IO.binread(file_path, 14, first_name_offset+32+i*46)
				s = ['Bonnar', 'Debra']
				if name_detected(s,t)
					puts "#{t}"
					IO.binwrite(file_path, "#{opic_id[0..3]}#{opic_id[-2..-1]}".ljust(14,' '), first_name_offset+32+i*46)
				end
			end
		end
		if first_comment_offset.present?
			first_comment_offset = content.index('ProtocolComment')
			(0..9).to_a.each do |i|
				t = IO.binread(file_path, 14, first_comment_offset+32+i*46)
				s = ['Bonnar', 'Debra']
				if name_detected(s,t)
					puts "#{t}"
					IO.binwrite(file_path, "#{opic_id[0..3]}#{opic_id[-2..-1]}".ljust(14,' '), first_comment_offset+32+i*46)
				end
			end
		end
	end

	def replace_ptn_name(file_path, name_tokens, opic_id)
		ptn_name_offset_1 = 9907
		ptn_name_offset_2 = 9903
		s = name_tokens
		t = IO.binread(file_path, 20, ptn_name_offset_1) 
		if name_detected(s,t)
			IO.binwrite(file_path, opic_id.ljust(20,' '), ptn_name_offset_1)
		else 
			t = IO.binread(file_path, 20, ptn_name_offset_2) 
			if name_detected(s,t)
				IO.binwrite(file_path, opic_id.ljust(20,' '), ptn_name_offset_2)
			end
		end
		# Montage name
		ptn_name_offset_2 = 129
		t = IO.binread(file_path, 16, ptn_name_offset_2) 
		# t is text to detect
		if name_detected(s,t)
			t = t.gsub(/\x00/, '')
			t = t.gsub(',', '').downcase[-1]
			IO.binwrite(file_path, "#{opic_id[0..3]}#{t}".ljust(16,' '), ptn_name_offset_2)
		end
	end

	def de_txt(file_path, first_name, last_name, local_patient_number, dob)
		#
		puts first_name
		puts last_name
		puts local_patient_number
		puts dob
		f = File.open(file_path, 'rb')
		content = f.read
		content = content.gsub(/#{first_name}/i, ' '*first_name.size)
		content = content.gsub(/#{last_name}/i, ' '*last_name.size)
		if dob.present?
			date_of_birth = Date.strptime(dob, "%Y%m%d")
			# MM/DD/YYYY
			format_1 = date_of_birth.strftime('%m/%d/%Y')
			# MM/DD/YY
			format_2 = date_of_birth.strftime("%m/%d/%y")
			# m/d/YYYY
			format_3 = date_of_birth.strftime('%-m/%-d/%Y')
			# m/dd/yyyy
			format_4 = date_of_birth.strftime('%-m/%d/%Y')
			# m/d/yy
			format_5 = date_of_birth.strftime('%-m/%-d/%y')
			# m/dd/yy
			format_6 = date_of_birth.strftime('%-m/%d/%y')


			content = content.gsub(format_1, ' '*10)
			content = content.gsub(format_2, ' '*8)
			content = content.gsub(format_3, ' '*8)
			content = content.gsub(format_4, ' '*9)
			content = content.gsub(format_5, ' '*6)
			content = content.gsub(format_6, ' '*7)
		end
		File.open(file_path, 'wb') do |f|
			f.puts content
		end
	end

	def replace_mrn(file_path, mrn, local_patient_number)
		file_format = file_path[-3..-1]
		mrn_offset = 48
		mrn_size = 12
		mrn_pnt_offset = 1540
		content = local_patient_number.ljust(mrn_size, ' ')
		IO.binwrite(file_path, content, mrn_offset)
		if file_format.upcase == 'PNT'
			IO.binwrite(file_path, content, mrn_pnt_offset)
		end
	end

	def format_date(excel_date)
		date_tokens = excel_date.split('/')
		month = date_tokens[0]
		if month.size == 1
			month = "0#{month}"
		end
		day = date_tokens[1]
		
		if day.size == 1
			day = "0#{day}"
		end

		year = date_tokens[2]
		if year.size == 2
			year = "20#{year}"
		end
		return "#{year}#{month}#{day}"
	end

	def name_detected(name_tokens, target_text)
	  # s = "#{Firstname}#{Lastname}"

		t = target_text.gsub(/\x00/, '')
		t = t.gsub(',', '').downcase
		t = t.gsub(' ', '')
		tokens = name_tokens.collect { |e| e.downcase  }
		tokens.each do |token|
			if t.include?(token)
				return true
			end
		end

		if t.include?(tokens.join) or t.include?(tokens.reverse.join) or t.include?(tokens.collect{|e| e[0]}.join) or t.include?(tokens.reverse.collect{|e| e[0]}.join)
			return true
		else 
			return false
		end
	end
end
