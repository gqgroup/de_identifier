class Patient < ApplicationRecord

	def self.find_by_name(name, date)
		if name.present? and date.present?
			tokens = name.split(',')
			tokens = tokens.collect(&:strip).collect(&:downcase)
			begin
				Date.strptime(date, '%Y%m%d')
			rescue => e
				puts e.message
				return nil
			end
			patient = Patient.where("last_name=? and first_name=? and doa<=? and dod>=?", tokens.first, tokens.last, date, date)
			# patient = Patient.where("last_name=? and first_name=?", tokens.first, tokens.last)
			if patient.size > 1
				p = patient.order('doa asc').first
				return p
			elsif patient.size == 1
				return patient.first
			else 
				return nil
			end
		else
			return nil
		end
	end

	def self.find_by_mrn(mrn, date)
		if mrn.present? and date.present?
			mrn = mrn.strip.gsub(/^0*/, '')
			begin
				Date.strptime(date, '%Y%m%d')
			rescue => e
				puts e.message
				return nil
			end
			patient = Patient.where("mrn=? and doa<=? and dod>=?", mrn, date, date)
			# patient = Patient.where("mrn=?", mrn)
			if patient.size > 1
				p = patient.order('doa asc').first
				return p
			elsif patient.size == 1
				return patient.first
			else 
				return nil
			end
		else
			return nil
		end
	end
end
