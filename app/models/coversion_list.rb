# == Schema Information
#
# Table name: coversion_lists
#
#  id         :bigint(8)        not null, primary key
#  site_name  :string(255)
#  opic_id    :string(255)
#  edf_plus   :boolean
#  edf        :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CoversionList < ApplicationRecord
	validates_uniqueness_of :opic_id
end
