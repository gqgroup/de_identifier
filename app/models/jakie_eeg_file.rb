class JakieEegFile < ApplicationRecord
	def de_identify
		phi_file_types = ["BFT", "CMT", "CN2", "EEG", "LOG", "PNT", 'EGF']
		name_tokens = []
		# file_path = self.path.gsub('NKT', 'NKT3/NKT')
		file_path = self.path
		# unless File.exists?(file_path)
		# 	return
		# end

		# unless phi_file_types.include?(self.path[-3..-1])
		# 	return
		# end

		opic_id = nil
		uh_number = nil
		first_name = nil
		last_name = nil
		dob = nil
		entry = self.path.split('/').last
		patient = nil
		if self.name.present? or self.mrn.present?
			patient = Patient.find_by_name(self.name, self.date)
			
			if patient.blank?
				patient = Patient.find_by_mrn(self.mrn, self.date)
			end
		end

		if patient.present?
			opic_id = "#{patient.opic_id}"
			uh_number = patient.uh_number
			first_name = patient.first_name
			last_name = patient.last_name
			name_tokens = [first_name, last_name]
			
			if patient.dob.present?
				dob = patient.dob.strftime("%Y%m%d")
			end
			begin 
				unless File.directory?(path)
					if entry.end_with?('PTN')
						replace_ptn_name(file_path, name_tokens, opic_id)
					elsif phi_file_types.include? entry[-3..-1]
						replace_name(file_path, name_tokens, opic_id)
						replace_mrn(file_path, mrn, uh_number)
					elsif entry[-3..-1] == 'EGF'
						de_egf(file_path, uh_number, opic_id)
					elsif entry[-3..-1] == 'LOG'
						de_txt(file_path, first_name, last_name, uh_number, dob);
					end
					self.dd = true
					self.patient_id = patient.id
					self.save
				end	
			rescue
				puts '=========='
				puts "#{file_path}"
				puts '=========='
			end
		end
	end

	def de_egf(file_path, uh_number, opic_id)
		f = File.open(file_path, 'rb')
		content = f.read
		content = content.gsub(/ID=(\w)+\n/, "ID=#{uh_number}\n")
		content = content.gsub(/PATNAME=[\w,\s\/]+\n/, "PATNAME=#{opic_id}\n")
		content = content.gsub(/BIRTHD=[\w,\s\/]+\n/, "BIRTHD=\n")
		File.open(file_path, "wb") {|file| file.puts content }
	end

	def replace_name(file_path, name_tokens, opic_id)

		file_format = file_path[-3..-1]	
		name_offset=79
		name_size=20
		content = opic_id.ljust(name_size, ' ')
		begin
			IO.binwrite(file_path, content, name_offset)
		rescue => e
			e.message
			puts '=========='
			puts "#{file_path}"
			puts '=========='
			return 
		end
		name_pnt_offset = 1582
		dob_pnt_offset = 1632
		dob_pnt_size = 10
		dob_de_content='01/01/0001'
		if file_format.upcase == 'PNT'
			begin
				IO.binwrite(file_path, content, name_pnt_offset)
				IO.binwrite(file_path, dob_de_content, dob_pnt_offset)
			rescue => e
				puts '=========='
				puts "#{file_path}"
				puts '=========='
			end
			replace_protocol_comment(file_path, name_tokens, opic_id)
		end
	end

	def replace_protocol_comment(file_path, name_tokens, opic_id)
		content = IO.binread(file_path)
		first_comment_offset = content.index('ProtocolComment')
		first_name_offset = content.index('ProtocolName')
		if first_name_offset.present?
			(0..9).to_a.each do |i|
				t = IO.binread(file_path, 14, first_name_offset+32+i*46)
				s = ['Bonnar', 'Debra']
				if name_detected(s,t)
					puts "#{t}"
					IO.binwrite(file_path, "#{opic_id[0..3]}#{opic_id[-2..-1]}".ljust(14,' '), first_name_offset+32+i*46)
				end
			end
		end
		if first_comment_offset.present?
			first_comment_offset = content.index('ProtocolComment')
			(0..9).to_a.each do |i|
				t = IO.binread(file_path, 14, first_comment_offset+32+i*46)
				s = ['Bonnar', 'Debra']
				if name_detected(s,t)
					puts "#{t}"
					IO.binwrite(file_path, "#{opic_id[0..3]}#{opic_id[-2..-1]}".ljust(14,' '), first_comment_offset+32+i*46)
				end
			end
		end
	end

	def replace_ptn_name(file_path, name_tokens, opic_id)
		ptn_name_offset_1 = 9907
		ptn_name_offset_2 = 9903
		s = name_tokens
		t = IO.binread(file_path, 20, ptn_name_offset_1) 
		if name_detected(s,t)
			IO.binwrite(file_path, opic_id.ljust(20,' '), ptn_name_offset_1)
		else 
			t = IO.binread(file_path, 20, ptn_name_offset_2) 
			if name_detected(s,t)
				IO.binwrite(file_path, opic_id.ljust(20,' '), ptn_name_offset_2)
			end
		end
		# Montage name
		ptn_name_offset_2 = 129
		t = IO.binread(file_path, 16, ptn_name_offset_2) 
		# t is text to detect
		if name_detected(s,t)
			t = t.gsub(/\x00/, '')
			t = t.gsub(',', '').downcase[-1]
			IO.binwrite(file_path, "#{opic_id[0..3]}#{t}".ljust(16,' '), ptn_name_offset_2)
		end
	end

	def de_txt(file_path, first_name, last_name, uh_number, dob)
		#
		puts first_name
		puts last_name
		puts uh_number
		puts dob
		f = File.open(file_path, 'rb')
		content = f.read
		content = content.gsub(/#{first_name}/i, ' '*first_name.size)
		content = content.gsub(/#{last_name}/i, ' '*last_name.size)
		if dob.present?
			date_of_birth = Date.strptime(dob, "%Y%m%d")
			# MM/DD/YYYY
			format_1 = date_of_birth.strftime('%m/%d/%Y')
			# MM/DD/YY
			format_2 = date_of_birth.strftime("%m/%d/%y")
			# m/d/YYYY
			format_3 = date_of_birth.strftime('%-m/%-d/%Y')
			# m/dd/yyyy
			format_4 = date_of_birth.strftime('%-m/%d/%Y')
			# m/d/yy
			format_5 = date_of_birth.strftime('%-m/%-d/%y')
			# m/dd/yy
			format_6 = date_of_birth.strftime('%-m/%d/%y')


			content = content.gsub(format_1, ' '*10)
			content = content.gsub(format_2, ' '*8)
			content = content.gsub(format_3, ' '*8)
			content = content.gsub(format_4, ' '*9)
			content = content.gsub(format_5, ' '*6)
			content = content.gsub(format_6, ' '*7)
		end
		File.open(file_path, 'wb') do |f|
			f.puts content
		end
	end

	def replace_mrn(file_path, mrn, uh_number)
		file_format = file_path[-3..-1]
		mrn_offset = 48
		mrn_size = 12
		mrn_pnt_offset = 1540
		content = uh_number.ljust(mrn_size, ' ')
		IO.binwrite(file_path, content, mrn_offset)
		if file_format.upcase == 'PNT'
			IO.binwrite(file_path, content, mrn_pnt_offset)
		end
	end

	def format_date(excel_date)
		date_tokens = excel_date.split('/')
		month = date_tokens[0]
		if month.size == 1
			month = "0#{month}"
		end
		day = date_tokens[1]
		
		if day.size == 1
			day = "0#{day}"
		end

		year = date_tokens[2]
		if year.size == 2
			year = "20#{year}"
		end
		return "#{year}#{month}#{day}"
	end

	def name_detected(name_tokens, target_text)
	  # s = "#{Firstname}#{Lastname}"

		t = target_text.gsub(/\x00/, '')
		t = t.gsub(',', '').downcase
		t = t.gsub(' ', '')
		tokens = name_tokens.collect { |e| e.downcase  }
		tokens.each do |token|
			if t.include?(token)
				return true
			end
		end

		if t.include?(tokens.join) or t.include?(tokens.reverse.join) or t.include?(tokens.collect{|e| e[0]}.join) or t.include?(tokens.reverse.collect{|e| e[0]}.join)
			return true
		else 
			return false
		end
	end
end
