json.extract! eeg_file, :id, :path, :type, :host, :first_name, :last_name, :middle_name, :date, :mrn, :de_identified, :created_at, :updated_at
json.url eeg_file_url(eeg_file, format: :json)
