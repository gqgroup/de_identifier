class EegFilesController < ApplicationController
  before_action :set_eeg_file, only: [:show, :edit, :update, :destroy]

  # GET /eeg_files
  # GET /eeg_files.json
  def index
    @eeg_files = EegFile.limit(200)
  end

  # GET /eeg_files/search
  # render index.html.erb

  def search
    if params[:name].blank?
      flash[:warning] = "Name cannot be blank for search"
      redirect_to root_path
      return
    end

    if params[:from_date].blank? or params[:to_date].blank?
      flash[:warning] = "Date cannot be blank for search"
      redirect_to root_path
      return
    end

    name_tokens = params[:name].strip.split
    begin
      from_date = Date.parse(params[:from_date]).strftime("%Y%m%d")
      to_date = Date.parse(params[:to_date]).strftime("%Y%m%d")
    rescue
      flash[:warning] = "Date format invalid: please use yyyy/mm/dd"
      redirect_to root_path      
      return
    end
    @name = params[:name]
    @from_date = params[:from_date]
    @to_date = params[:to_date]

    @eeg_files = EegFile.search_around(name_tokens, from_date, to_date)
    
    render 'index'
  end

  # GET /eeg_files/1
  # GET /eeg_files/1.json
  def show
  end

  # GET /eeg_files/new
  def new
    @eeg_file = EegFile.new
  end

  # GET /eeg_files/1/edit
  def edit
  end

  # POST /eeg_files
  # POST /eeg_files.json
  def create
    @eeg_file = EegFile.new(eeg_file_params)

    respond_to do |format|
      if @eeg_file.save
        format.html { redirect_to @eeg_file, notice: 'Eeg file was successfully created.' }
        format.json { render :show, status: :created, location: @eeg_file }
      else
        format.html { render :new }
        format.json { render json: @eeg_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /eeg_files/1
  # PATCH/PUT /eeg_files/1.json
  def update
    respond_to do |format|
      if @eeg_file.update(eeg_file_params)
        format.html { redirect_to @eeg_file, notice: 'Eeg file was successfully updated.' }
        format.json { render :show, status: :ok, location: @eeg_file }
      else
        format.html { render :edit }
        format.json { render json: @eeg_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /eeg_files/1
  # DELETE /eeg_files/1.json
  def destroy
    @eeg_file.destroy
    respond_to do |format|
      format.html { redirect_to eeg_files_url, notice: 'Eeg file was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_eeg_file
      @eeg_file = EegFile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def eeg_file_params
      params.require(:eeg_file).permit(:path, :file_type, :host, :name, :date, :mrn, :de_identified)
    end
end
