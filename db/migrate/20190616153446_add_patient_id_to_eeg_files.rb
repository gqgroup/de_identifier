class AddPatientIdToEegFiles < ActiveRecord::Migration[5.2]
  def change
    add_column :eeg_files, :patient_id, :integer
  end
end
