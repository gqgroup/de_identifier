class AddPatientIdToJakieEegFiles < ActiveRecord::Migration[5.2]
  def change
    add_column :jakie_eeg_files, :patient_id, :integer
  end
end
