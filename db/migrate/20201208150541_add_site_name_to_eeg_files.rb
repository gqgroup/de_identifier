class AddSiteNameToEegFiles < ActiveRecord::Migration[5.2]
  def change
    add_column :eeg_files, :site_name, :string
  end
end
