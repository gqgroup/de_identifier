class CreateCoversionLists < ActiveRecord::Migration[5.2]
  def change
    create_table :coversion_lists do |t|
      t.string :site_name
      t.string :opic_id
      t.boolean :edf_plus
      t.boolean :edf

      t.timestamps
    end
  end
end
