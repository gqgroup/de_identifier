class ChangePatientsTable < ActiveRecord::Migration[5.2]
  def change
  	rename_column :patients, :uh_number, :local_patient_number 
  	add_column :patients, :site_name, :string
  end
end
