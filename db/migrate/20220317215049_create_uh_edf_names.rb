class CreateUhEdfNames < ActiveRecord::Migration[5.2]
  def change
    create_table :uh_edf_names do |t|
      t.string :opic_id
      t.string :data_name

      t.timestamps
    end
  end
end
