class CreateJakieEegFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :jakie_eeg_files do |t|
      t.string :path
      t.string :file_type
      t.string :name
      t.string :mrn
      t.string :date
      t.boolean :dd

      t.index ["name", "mrn"], name: "index_eeg_files_on_name_and_mrn"
      t.timestamps
    end
  end
end
