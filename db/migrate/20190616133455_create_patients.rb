class CreatePatients < ActiveRecord::Migration[5.2]
  def change
    create_table :patients do |t|
      t.string :uh_number
      t.string :opic_id
      t.string :mrn
      t.string :first_name
      t.string :last_name
      t.date :doa
      t.date :dod
      t.date :dob

      t.timestamps
    end
  end
end
