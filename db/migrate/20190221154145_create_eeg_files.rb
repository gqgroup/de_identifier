class CreateEegFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :eeg_files do |t|
      t.string :path
      t.string :file_type
      t.string :host
      t.string :name
      t.string :date
      t.string :mrn
      t.boolean :de_identified

      t.timestamps
    end
  end
end
