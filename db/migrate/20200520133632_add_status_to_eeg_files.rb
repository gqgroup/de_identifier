class AddStatusToEegFiles < ActiveRecord::Migration[5.2]
  def change
    add_column :eeg_files, :edf_plus, :boolean
    add_column :eeg_files, :edf, :boolean
  end
end
