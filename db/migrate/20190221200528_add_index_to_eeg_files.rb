class AddIndexToEegFiles < ActiveRecord::Migration[5.2]

  def change
  	add_index :eeg_files, [:name, :mrn]
  end
end
